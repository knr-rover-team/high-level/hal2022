from pathlib import Path
import os
import shutil


YAML: list = ["zed2.yaml", "common.yaml", "sync.yaml"]

def get_paths() -> tuple:
    start_path = Path("/home")
    zed_wrapper_path = list(start_path.glob("**/src/zed-ros-wrapper/zed_wrapper/params"))[0]
    zed_hal_config_path = list(start_path.glob("**/hal2022/zed_yaml"))[0]
    print(f"====\nzed_wrapper_path == {zed_wrapper_path} \nzed_hal_config_path == {zed_hal_config_path}\n====")
    return (zed_wrapper_path, zed_hal_config_path)

def replace_yaml_files(source: str, destination: str) -> None:
    global YAML
    for yaml in YAML:
        shutil.copyfile(os.path.join(source, yaml),os.path.join(destination, yaml))
        print(f"YAML_FILE == {yaml} :: REPLACED")
    print("ALL FILES REPLACED")



def main():
    try:
        (zed_wrapper_path, zed_hal_config_path) = get_paths()
        replace_yaml_files(source=zed_hal_config_path, destination=zed_wrapper_path)
    except Exception as e:
        print(e)
        
if __name__ == "__main__":
    main()


