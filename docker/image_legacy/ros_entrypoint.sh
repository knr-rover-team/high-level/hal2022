#!/bin/bash

echo "##### ROS is setup ####"

echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
echo "source ~/ros-humble-ros1-bridge/install/local_setup.bash" >> ~/.bashrc
echo "source ~/ros2_docker_ws/install/setup.bash" >> ~/.bashrc

exec bash