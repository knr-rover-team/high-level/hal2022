#!/bin/bash

echo "##### ROS is setup ####"

echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
echo "source /root/bridge_ws/install/local_setup.bash" >> ~/.bashrc
echo "source /root/ros2_docker_ws/install/setup.bash" >> ~/.bashrc

exec bash
