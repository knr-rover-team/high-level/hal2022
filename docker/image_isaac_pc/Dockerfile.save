# Use the base ROS 2 isaac image
FROM isaac_ros_dev-x86_64

#Keys
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null \
    && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null


# Update
RUN apt-get update -y

# Install additional packages and software, e.g.
RUN apt-get install -y \
    ros-humble-robot-localization \
    libboost-all-dev \
    ros-humble-rclcpp \
    tilix \
    nano \
    && rm -rf /var/lib/apt/lists/*

# Install stuff for noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-noetic.list'
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
RUN apt-get update -y
RUN apt-get install -y ros-noetic-ros-base
RUN apt-get install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install all CUDA
RUN apt-get update -y
RUN apt-get install -y cuda-11-8
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install isaac_ros things for VSLAM
RUN apt-get update -y \
    && apt-get install -y \
    ros-humble-isaac-ros-visual-slam \
    ros-humble-isaac-ros-image-proc \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 uninstall -y setuptools-scm

COPY ros_entrypoint.sh /ros_entrypoint.sh
RUN chmod +x /ros_entrypoint.sh

# Setup the entrypoint
ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["bash"]
