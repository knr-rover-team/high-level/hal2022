#!/usr/bin/env python3

import launch, os
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import ComposableNodeContainer, Node
from launch_ros.descriptions import ComposableNode
from launch.substitutions import LaunchConfiguration
from launch.substitutions import TextSubstitution


"""THIS LAUNCHFILE IS FOR ZED-ONLY TESTS"""
def generate_launch_description():


    """ISAAC_ROS_VISUAL_SLAM  <1>"""
    visual_slam_node=ComposableNode(
        name='visual_slam_node',
        package='isaac_ros_visual_slam',
        plugin='nvidia::isaac_ros::visual_slam::VisualSlamNode',
        parameters=[{
                    'enable_image_denoising': False,
                    'rectified_images': True,
                    'enable_imu_fusion': True,
                    'image_jitter_threshold_ms': 105.00,  
                    'base_frame': 'base_link',
                    'imu_frame': 'zed2_imu_link',
                    'enable_slam_visualization': True,
                    'enable_landmarks_view': True,
                    'enable_observations_view': True,
                    'camera_optical_frames': [
                        'zed2_left_camera_optical_frame',
                        'zed2_right_camera_optical_frame',
                    ],
                    }],
        remappings=[('visual_slam/image_0', '/zed2/zed_node/left/image_rect_rgb'),
                     ('visual_slam/camera_info_0', '/zed2/zed_node/left/camera_info'),
                     ('visual_slam/image_1', '/zed2/zed_node/right/image_rect_rgb'),
                     ('visual_slam/camera_info_1', '/zed2/zed_node/right/camera_info'),
                     ('visual_slam/imu', '/zed2/zed_node/imu/data')]
    )

    """IMAGE CONVERSION<1>"""
    image_format_converter_node_left = ComposableNode(
        package='isaac_ros_image_proc',
        plugin='nvidia::isaac_ros::image_proc::ImageFormatConverterNode',
        name='image_format_node_left',
        parameters=[{'encoding_desired': 'rgb8',
                     'image_width': 1280,
                     'image_height': 720
                     }],
        remappings=[
            ('image_raw', '/zed2/zed_node/left/image_rect_color'),
            ('image', '/zed2/zed_node/left/image_rect_rgb')]
    )

    """IMAGE CONVERSION<2>"""
    image_format_converter_node_right = ComposableNode(
        package='isaac_ros_image_proc',
        plugin='nvidia::isaac_ros::image_proc::ImageFormatConverterNode',
        name='image_format_node_right',
        parameters=[{'encoding_desired': 'rgb8',
                     'image_width': 1280,
                     'image_height': 720
                     }],
        remappings=[
            ('image_raw', '/zed2/zed_node/right/image_rect_color'),
            ('image', '/zed2/zed_node/right/image_rect_rgb')]
    )


    """ISAAC_ROS_VISUAL_SLAM  <2>"""
    visual_slam_launch_container = ComposableNodeContainer(
        name='visual_slam_launch_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
            image_format_converter_node_left,
            image_format_converter_node_right,
            visual_slam_node],
        output='screen'
    )

    """RVIZ2"""
    config = "~/ros2_docker_ws/src/hal_vslam/rviz/slam_config_rviz.rviz"
    rviz2 = Node(
        name="rviz2",
        package="rviz2",
        executable="rviz2",
        output="screen",
        arguments=['-d',config]
    )


    return launch.LaunchDescription([visual_slam_launch_container, rviz2])