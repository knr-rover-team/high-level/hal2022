from launch import LaunchDescription
from launch_ros.actions import Node

import launch_ros
import os

def generate_launch_description():
    pkg_share = launch_ros.substitutions.FindPackageShare(package='hal_main').find('hal_main')
    ekf_config_path = os.path.join(pkg_share, 'config', 'ekf.yaml')

    fake_odom_node = Node(
        package='hal_main',
        executable='fake_odom.py',
        name='fake_odom'
    )

    robot_localization_node = Node(
        package='robot_localization',
        executable='ekf_node',
        name='ekf_filter_node',
        output='screen',
        parameters=[ekf_config_path, {'use_sim_time': False}]
    )

    return LaunchDescription([
        fake_odom_node,
        robot_localization_node
    ])