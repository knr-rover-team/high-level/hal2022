from launch import LaunchDescription
import launch_ros
from launch_ros.actions import Node

def launch_description():
    return LaunchDescription([
        Node(
            package='nav2_bringup',
            executable='bringup_launch',
            output='screen',
            parameters=[
                'config/nav2_params.yaml'
            ]
        ),
        Node(
            package='nav2_map_server',
            executable='map_server',
            output='screen',
            parameters=[
                'maps/costmap.yaml'
            ]
        )
    ])