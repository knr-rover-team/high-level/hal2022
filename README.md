# HAL-062 repository
HAL-062 is a project of martian rover developed in the Students' Robotics Association at the Faculty of Power and Aeronautical Engineering of the Warsaw University of Technology (WUT). This repository contains software that implements autonomous behaviour for the robot. It is prepared taking into account requirements for University Rover Challenge (URC) and European Rover Challenge (ERC) competitions.

Installation:
clone
sudo apt-get install ros-noetic-hector-gazebo-plugins

Launch:
- modern with sim: roslaunch hal_launch rover_and_simulation.launch 
- modern without sim: roslaunch hal_launch rover.launch 
- legacy: roslaunch hal_navigation navigation.launch world_type:=hal_world_bumpy_ar

To start driving send command, eg.:
rostopic pub -1 /hal_commands std_msgs/String -- START


To use the IMU:

1st time:
sudo apt-get install ros-noetic-um7
sudo usermod -a -G dialout $USER
reboot

always:
rosrun um7 um7_driver _port:=/dev/ttyUSB0

To use ROS2:

- install Docker: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

- install container: https://nvidia-isaac-ros.github.io/getting_started/index.html from https://github.com/NVIDIA-ISAAC-ROS/isaac_ros_common. From the second link: use release 2.1.0 (git checkout v2.1.0) for compatibility with Jetson. Then, clone and run scripts/run_dev.sh

- build the container: 
  go to selected config folder: {repo_path}/docker/{config_name} 
  - image_legacy for installs with the old version of the manual (isaac 3.0).
  - image configured for nvidia + old version of the manual (isaac 3.0)
  - image_isaac for curent jetson setup
  - image_isaac_pc for running on pc with isaac 2.1.0 and NV gpu
  docker build -t ros2_humble_configured .

- to build the bridge (once - you get a prebuilt package)
  - Run the container (using command in the chapter below but do not share bridge folder). 
  - Then install ros humble from source required packages: https://docs.ros.org/en/humble/Installation/Alternatives/Ubuntu-Development-Setup.html#using-the-ros-1-bridge - up to (including) the "Install dependencies using rosdep" chapter BUT DO NOT RUN THE APT UPGRADE COMMAND. use Ubuntu 20.04 instructions variant.
 - remove all "source" commands from the bottom of ~/.bashrc and start a new docker terminal with docker exec
 - in that new terminal, build the bridge using the tutorial: https://github.com/jayprajapati009/ros1_bridge_tutorial - during the instructions use ros2 sourced from /opt/ros/humble
 - e.g. using docker cp command, download the built bridge in bridge_ws folder to host machine and save as imported_bridge_ws, e.g.: docker cp ros2_humble_container:/home/hal/bridge_ws /home/hal/imported_bridge_ws

- install docker extension (rocker):
  sudo apt install python3-rocker

- run the container: 
```
rocker --user --x11 --network=host --volume /dev/shm:/dev/shm --volume {repo path}/ros2_docker_ws:/home/$USER/ros2_docker_ws --volume {bridge path}:/home/$USER/bridge_ws --name ros2_humble_container {image name}
```
e.g. 
```
rocker --user --x11 --network=host --volume /dev/shm:/dev/shm --volume ~/ros_hal_2022/ros2_docker_ws:/home/$USER/ros2_docker_ws --volume ~/imported_bridge_ws:/home/$USER/bridge_ws --name ros2_humble_container ros2_humble_configured
```
for nvidia & jetson (CANNOT USE USER). config for building the proper image - on vslam branch (sry)
```
rocker --x11 --network=host --nvidia=runtime --volume /dev/shm:/dev/shm --volume ~/ros_hal_2022/ros2_docker_ws:/root/ros2_docker_ws --volume ~/imported_bridge_ws:/root/bridge_ws --name ros2_humble_container ros2_humble_isaac
```
for PC with isaac (issue here. U need to source the bridge **manualy** before running it. thats it.):
```
rocker --x11 --network=host --nvidia=gpus --volume /dev/shm:/dev/shm --volume ~/ros_hal_2022/ros2_docker_ws:/root/ros2_docker_ws --volume ~/imported_bridge_ws:/root/bridge_ws --name ros2_humble_container ros2_humble_isaac
```
for Miłosz:
```
rocker --interactive --nvidia=gpus --x11 --network=host --volume /dev/shm /dev/shm --volume ~/ros_hal_2022/ros2_docker_ws:/root/ros2_docker_ws --volume ~/ros-humble-ros1-bridge:/root/ros-humble-ros1-bridge --name ros2_humble_container ros2_humble_configured
```

- warning: only things in shared directory will be maintained after closing the container (because of rocker). If you want to maintain e.g. installation of something, put in in the dockerfile.

- to get extra terminal for the same container:
  docker exec -it ros2_humble_container bash



Usefull stuff:
  -setup package for ros2 with both python and cpp: https://roboticsbackend.com/ros2-package-for-both-python-and-cpp-nodes/