# Communication standard 
Communication inside rover is compliant with CAN standard. User can send UART frame with some ID to mainboard that broadcasts corresponding CAN frame. UART frame consists of 19 8-bit signs and constant length. uint8_t signs are sent in HEX representation as chars, so each number takes 2 signs.

If singed int is sent it should be sent as uint8_t and reinterpreted us signed on board that is receiving the appropriate frame.

Frame is organized as follows:
- data[0] - '#'(ASCII - 35) - designation of frame beginning
- data[1-2] - frame ID (0-255)
- data[3-18] - data: 8 fields of 8-bit

If frame has less than 8 signs remaining fields are filled with sign 'x'.

Example:
- ID = 71
- frame length - 3 signs
- data[0...2] = {0,11,253}

Resulting frame:
- #47000BFDxxxxxxxxxx

Frames IDs for modules of rover are divided in following manner:
- Platform 0-127
- Manipulator 128-191
- Lab 192-255

# Platform (ID 0-127)
## Motor controllers (ID 20-39)
1. Switching the engines on and off (START = 0x01, STOP = 0x00 but anything other than 0x01 will be interpreted as STOP). START/STOP segment is repeated 3 times for safety
- ID = 20
- data[0] - START/STOP 
- data[1] - START/STOP
- data[2] - START/STOP
- data[3] - max current (in percent [0-100] <-> [0A-xxA])
- data[4] - max speed (in percent [0-100] <-> [0A-xxA])

2. Reference speed for right side
- ID = 21
- data[0] - speed right front 
- data[1] - speed right middle
- data[2] - speed right rear

3. Reference speed for left side
- ID = 22
- data[0] - speed left front 
- data[1] - speed left middle
- data[2] - speed left rear

4. Reset of driver ?????
- ID - 28
- data[0] 
		data = 0  - nic nie robi
		data = 1  - resetuje statusy b��d�w
		data = 2  - prze��cza w tryb pracy bez sprz�enie od pr�dko�ci (PWM MODE)
		data = 3  - resetuje ca�y sterownik

## Blinking LEDs

# Manipulator
Manipulator modes:
-  0  - disable
-  1  - hold
-  2  - degrees
-  3  - joint speeds
-  4  - xyz... global
-  5  - speed global
-  6  - speed tool
-  7  - delta tool position
-  8  - speed global without 6
-  9  - speed tool without 6
-  10  - delta tool position without 6
-  11  - speed global 123, 456 - joint speeds
-  12  - speed tool 123, 456 - joint speeds

## External frames:
1. Switching the manipulator on and off (START = 0x01, STOP = 0x00 but anything other than 0x01 will be interpreted as STOP). START/STOP segment is repeated 3 times for safety
- ID - 128
- data[0] - START/STOP
- data[1] - START/STOP
- data[2] - START/STOP
- data[3]-data[4] - Mode
- data[5]-data[6] - 0 = normal, 1 = DoF6 speed mode, 2 = DoF456 speed mode  <!-- 2 = DoF456 speed mode !! has to be checked !! -->

2. Frame from operator 
- ID - 129
- data [0] - angle1 - MSB 
- data [1] - ...
- data [2] - ...
- data [3] - angle1 - LSB
- data [4] - angle2 - MSB 
- data [5] - ...
- data [6] - ...
- data [7] - angle2 - LSB
- ID - 130
- data [0] - angle3 - MSB 
- data [1] - ...
- data [2] - ...
- data [3] - angle3 - LSB
- data [4] - angle4 - MSB 
- data [5] - ...
- data [6] - ...
- data [7] - angle4 - LSB
- ID - 131
- data [0] - angle5 - MSB 
- data [1] - ...
- data [2] - ...
- data [3] - angle5 - LSB
- data [4] - angle6 - MSB 
- data [5] - ...
- data [6] - ...
- data [7] - angle6 - LSB

2. Measurement data (joint angles) can be retrieved by the operator. Data frame is described in Internal manipulator frames (IDs: 157-162) 

## Internal manipulator frames (ID: 141-191)
1. Frame from concentrator to every DoF and gripper
- ID - 151-157
- data [0] - start/stop 
- data [1] - start/stop
- data [2] - mode
- data [3] - mode
- data [4] - data - MSB 
- data [5] - ...
- data [6] - ...
- data [7] - data - LSB

2. Frame from every DoF to concentrator with measurement of joint position (DoF angle)
- ID - 158-163
- data [0] - angle - MSB 
- data [1] - ...
- data [2] - ...
- data [3] - angle - LSB
