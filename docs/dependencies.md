Versions of the used software:
| Dependency  | Version       |
| ----------- | ------------- |
| ROS         | Noetic        |
| C++         | C++17 standard|
| Python      | 3.8           |
| OpenCv      | -             |
