The software is base on ROS Noetic, C++ (C++17) and Python 3.8. The software consists of multiple modules. Each one of them implements separate functionality that is used by the rover's software.

# hal_navigation
Package containing A* algorith for determining the most optimal path to the waypoints.

# hal_gps
Package providing GPS data coming from physical device (TODO: name of the integrated circuit).

# hal_imu
Package providing IMU data coming from physical device (TODO: name of the integrated circuit).

# hal_odometry
Package providing odometry data from sensors attached to the axes of the rover's wheels.

# hal_ar_position
Package that uses images to extract rover's position relative to the AR tags. 

# hal_ar_codes_recognition
Package that detects and recognizes AR tags. 

# hal_kalman
Package that implements Kalman filter to provide reliable data about rover's position and orientation in global reference frame.

# hal_obstacle_detector
Package that uses Point Cloud Library (PCL) algorithms to process ZED2 depth camera images. It provides estimated position and size of encountered obstacles.

# hal_manipulator
Package that uses MoveIt library to provide autonomuos behaviout to the robotic manipulator mouned on the rover. It implements inverse kinematics and path palnning algorithms.

# hal_rover_simulator
Package that handles simulation environment of the rover (models, terrain etc.).  

# hal_communication
Package for handling communication with the mainboard

# hal_coordinate_estimator
Package that published poses based on data from simulated sensors. To be replaced with more sophisticated solution

# hal_gazebo_2_rviz_spy
A quick fix for rviz to work while running simulation, publishing poses

# hal_launch
Main launch to switch sim / real rover

# hal_meshes
Some meshes are here, some in meshes_gazebo. Mostly used are the ones in meshes_gazebo, but hal_meshes may still be referenced (TODO)

# hal_msgs
Msgs and srvcs



