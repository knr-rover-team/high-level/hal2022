#!/usr/bin/env python3
import socket
import threading
import datetime
import time

# Robot connection
robot_ip = "192.168.1.88"
# robot_ip = "192.168.1."
robot_port = 5000
robot_socket = None
mutex = threading.Lock()

def handle_client(client_socket):
    global robot_socket, mutex
    while True:
        try:
            data = client_socket.recv(1024)
            if not data:
                continue
            timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print(f'\r{timestamp}', end=' ', flush=True)
            print("data sending")
            mutex.acquire()
            robot_socket.sendall(data)
            time.sleep(1e-2)
            mutex.release()
            # response = robot_socket.recv(1024)    #no receive
            # client_socket.sendall(response)
        except Exception as e:
            print(e)
            break
    client_socket.close()

def start_relay_server():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('0.0.0.0', 8888))
    server.listen(5)
    print("Relay server started on port 8888")

    while True:
        client_socket, addr = server.accept()
        print(f"Accepted connection from {addr}")
        client_handler = threading.Thread(target=handle_client, args=(client_socket,))
        client_handler.start()

def start_robot_connection():
    global robot_socket
    robot_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    robot_socket.connect((robot_ip, robot_port))
    print("robot connected")

if __name__ == "__main__":
    start_robot_connection()
    start_relay_server()