#!/usr/bin/env python3
import socket
import time,os,pygame
import pyzed.sl as sl
import numpy as np

#######################################################
## - Check camera connection                         ##
## - Check if pyzed is installed and works properly  ##
## - Turn off zed_wrapper                            ##
#######################################################

HOST = "0.0.0.0"
PORT = 8100
JPG = "science_task.jpg"

def open_camera() -> sl.Camera:
    try:
        camera = sl.Camera()
        init_params = sl.InitParameters()
        init_params.camera_resolution = sl.RESOLUTION.HD720
        init_params.camera_fps = 30
        init_params.depth_mode = sl.DEPTH_MODE.NONE

        camera.open(init_params)
        if(not camera.is_opened):
            raise TypeError
        
    except Exception as e:
        print(e)

    return camera
        
def take_a_photo(camera : sl.Camera) -> str:
    try:
        pygame.init()
        runtime_params = sl.RuntimeParameters(enable_depth=False)
        image = sl.Mat(mat_type=sl.MAT_TYPE.U8_C1)

        if camera.grab(runtime_params) == sl.ERROR_CODE.SUCCESS:
            camera.retrieve_image(image,sl.VIEW.LEFT)
            np_image = image.numpy()
        else:
            raise TypeError
        
        window = pygame.display.set_mode((1280,720))
        for i in range(0,window.get_size()[0]):
            for j in range(0,window.get_size()[1]):
                pixel = (np_image[j][i])
                color = pygame.Color(int(pixel[2]),int(pixel[1]),int(pixel[0]),int(pixel[3]))
                window.set_at((i,j),color)
        pygame.display.flip()
        pygame.image.save(window, JPG)
        path = os.path.abspath(JPG) 
        del window

    except Exception as e: 
        print(e)
    
    finally:
        pygame.quit()
        camera.close()

    return path

# TCP | SERVER INIT
if __name__ == "__main__":
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((HOST,PORT))
    server.listen(3)

    while True:
        print("[Waiting for a photo request]\n")
        [client_socket,address] = server.accept()

        print("[preparation-stage...]\n")
        camera = open_camera()
        photo_path = take_a_photo(camera)

        print("[sending-stage...]\n")
        with open(photo_path,"rb") as photo:
            file_data = photo.read()
            try:
                client_socket.sendall(file_data)
                client_socket.send(b"<end>")
                print("[!DONE!]\n")
            except:
                print("[connection has been lost]\n")
        if os.path.exists(photo_path):
            os.remove(photo_path)
        time.sleep(1)
        client_socket.close()
        break
    server.close()

