//HAL autonomy
//A* algorithm


#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <list>
#include <iomanip>
#include <chrono>
#include <fstream>
#include <sstream>


#include "ros/ros.h"
#include "ros/package.h"
#include "geometry_msgs/Pose2D.h"
#include "geometry_msgs/Quaternion.h"   //used as 2 points, not quaternion
#include "std_msgs/String.h"


#define PRINT_FULL


using namespace std;




struct MapPoint{
    uint8_t cost;   //for obstacle map
    uint16_t toward;//cost to drive towards
    uint16_t heur;  //total heur
    int dist;
    int y_v;        //via
    int x_v;
    int heading_v;  //rover orientation when approaching the object
    bool visited;
    bool path;

    MapPoint(uint8_t cost):
    cost(cost),
    toward(numeric_limits<uint16_t>::max()),
    heur(numeric_limits<uint16_t>::max()),
    dist(0),
    y_v(-1),
    x_v(-1),
    heading_v(-1),
    visited(0),
    path(0)
    {}
};



class NavMap{
    public:
        int size_y;
        int size_x;
        vector<vector<MapPoint>> map;
        list<pair<int, int>> avail;
        int y_t;
        int x_t;
        
        int y_first_step;
        int x_first_step;
        int y_second_step;
        int x_second_step;
        int y_third_step;
        int x_third_step;

        int y_last_start;
        int x_last_start;

        void call_a_star(const geometry_msgs::Pose2D::ConstPtr&);

        NavMap();

        //Main function to run the astar algorithm once
        void a_star(int y_0, int x_0, int heading_0);
        //calculate the remaining destince to goal from argument coordinates
        int remain_d(int y_n, int x_n);
        //calculates the heuristics - remaining distance for each cell
        void compute_dist();
        //working function for astar, from given coordinates pushes neighbours on point queue
        void load_avail(int y_n, int x_n);
        //generates the best path after astar completes calculations
        void retrace();
        //calculates heading in 8 direction notation from x_n, y_n to x,y
        int get_heading(int y_n, int x_n, int y, int x);
        //set astar global goal
        void set_target(int y_targ, int x_targ);
        //resets the map and precomputed values that optimize the algorithm
        void purge();

        friend ostream& operator<< (ostream & os, const NavMap & N);
        friend class AStarPubSub;
};


NavMap::NavMap() {
    // MapPoint struct_0(1);
    // map.resize(size_y, vector<MapPoint>(size_x, struct_0));
    fstream map_file;
    const string pkg_path = ros::package::getPath("hal_navigation");
    map_file.open(pkg_path+"/scripts/terrain_nav_raw.csv", ios::in);
    if(map_file.is_open())
	{
        cout<<"map opened"<<endl;
        string csv_line, s_val;
        vector<MapPoint> row;
        while(map_file>>csv_line)
		{
            row.clear();
            stringstream csv_str(csv_line);
			while(getline(csv_str, s_val, ';')){
                int s_cost = stoi(s_val);
                row.push_back(MapPoint(s_cost));
            }
			map.push_back(row);
		}
        size_y = map.size();
        size_x = map[0].size();
        y_last_start=0;
        x_last_start=0;

        map_file.close();
        cout<<"map loaded"<<endl;
    }
	else{
        cout<<"map file err"<<endl;
        throw std::invalid_argument("No file");
    }
}


ostream& operator<< (ostream & os, const NavMap & N){

    #ifdef PRINT_FULL


    for(int j=0; j<N.size_y; j++){
        for(int i=0; i<N.size_x; i++){
            if(N.map[j][i].path==0)
                os<<left<<setw(4)<<short(N.map[j][i].cost);
            else os<<left<<setw(4)<<"X";
        }
        os<<endl;
    }

    #endif


    return os;
}


void NavMap::a_star(int y_0, int x_0, int heading_0){
    y_last_start=y_0;
    x_last_start=x_0;
    purge();
    avail.push_front(make_pair(y_0, x_0));
    map[y_0][x_0].toward=0;
    map[y_0][x_0].heur=0;
    int y = y_0;
    int x = x_0;
    map[y_0][x_0].heading_v = heading_0;
    while(y!=y_t || x!=x_t){
        y = avail.front().first;
        x = avail.front().second;
        if(map[y][x].visited){
            avail.pop_front();
            continue;
        }
        else{
            avail.pop_front();
            load_avail(y, x);
        }
    }
    retrace();
}


int NavMap::remain_d(int y_n, int x_n){
    return( sqrt(pow(y_t-y_n, 2) + pow(x_t-x_n, 2)) );
    //return(abs(y_t-y_n)+abs(x_t-x_n));    //taxicab geometry
}


int NavMap::get_heading(int y_n, int x_n, int y, int x){
    //heading compass:
    // |7|0|1|     N
    // |6|X|2|   W + E
    // |5|4|3|     S

    if(x==x_n){
        if(y==y_n-1) return 0;
        else return 4;
    }
    else if(x==x_n+1){
        if(y==y_n-1) return 1;
        else if(y==y_n) return 2;
        else return 3;
    }
    else {
        if(y==y_n-1) return 7;
        else if(y==y_n) return 6;
        else return 5;
    }
}


void NavMap::compute_dist(){
    for(int j=0; j<size_y; j++){
        for(int i=0; i<size_x; i++){
            map[j][i].dist=remain_d(j, i);
        }
    }
}


void NavMap::set_target(int y_targ, int x_targ){
    y_t = y_targ;
    x_t = x_targ;
}


void NavMap::load_avail(int y_n, int x_n){
    vector<pair<int, int>> around;

    //neighbouring
    if(y_n-1>=0) around.push_back(make_pair(y_n-1, x_n));
    if(y_n+1<size_y) around.push_back(make_pair(y_n+1, x_n));
    if(x_n-1>=0) around.push_back(make_pair(y_n, x_n-1));
    if(x_n+1<size_x) around.push_back(make_pair(y_n, x_n+1));
    for(unsigned int i=0; i<around.size(); i++){
        int y = around[i].first;
        int x = around[i].second;
        if(!map[y][x].visited){
            int heading_to = get_heading(y_n, x_n, y, x);
            int heading_diff = abs(map[y_n][x_n].heading_v - heading_to);
            heading_diff = min(heading_diff, 8-heading_diff);
            int toward_now = map[y_n][x_n].toward + map[y][x].cost + map[y_n][x_n].cost*pow(heading_diff, 2)/2;
            int heur_now = toward_now + map[y][x].dist;
            if(heur_now < map[y][x].heur){
                map[y][x].toward = toward_now;
                map[y][x].heur = heur_now;
                map[y][x].y_v = y_n;
                map[y][x].x_v = x_n;
                map[y][x].heading_v = heading_to;
                //load to queue
                bool inserted = false;
                for(auto it=avail.begin(); it!=avail.end(); it++){
                    if(map[it->first][it->second].heur > heur_now){
                        avail.insert(it, make_pair(y, x));
                        inserted = true;
                        break;
                    }
                }
                if(!inserted) avail.push_back(make_pair(y, x));
            }
        }
    }

    //diagonal
    around.clear();
    if(y_n-1>=0 && x_n-1>=0) around.push_back(make_pair(y_n-1, x_n-1));
    if(y_n-1>=0 && x_n+1<size_x) around.push_back(make_pair(y_n-1, x_n+1));
    if(y_n+1<size_y && x_n-1>=0) around.push_back(make_pair(y_n+1, x_n-1));
    if(y_n+1<size_y && x_n+1<size_x) around.push_back(make_pair(y_n+1, x_n+1));
    for(unsigned int i=0; i<around.size(); i++){
        int y = around[i].first;
        int x = around[i].second;
        if(!map[y][x].visited){
            int avg_cost = (2*map[y][x].cost + map[y_n][x].cost + map[y][x_n].cost)*1.5/4;  //sqrt(2)
            int heading_to = get_heading(y_n, x_n, y, x);
            int heading_diff = abs(map[y_n][x_n].heading_v - heading_to);
            heading_diff = min(heading_diff, 8-heading_diff);
            int toward_now = map[y_n][x_n].toward + avg_cost + map[y_n][x_n].cost*pow(heading_diff, 2)*2; //was /2
            int heur_now = toward_now +  map[y][x].dist;
            if(heur_now < map[y][x].heur){
                map[y][x].toward = toward_now;
                map[y][x].heur = heur_now;
                map[y][x].y_v = y_n;
                map[y][x].x_v = x_n;
                map[y][x].heading_v = heading_to;
                //load to queue
                bool inserted = false;
                for(auto it=avail.begin(); it!=avail.end(); it++){
                    if(map[it->first][it->second].heur > heur_now){
                        avail.insert(it, make_pair(y, x));
                        inserted = true;
                        break;
                    }
                }
                if(!inserted) avail.push_back(make_pair(y, x));
            }
        }
    }

    map[y_n][x_n].visited = true;
}


void NavMap::retrace(){
    int y = y_t;
    int x = x_t;
    y_third_step = y_t;
    x_third_step = x_t;
    y_second_step = y_t;
    x_second_step = x_t;
    y_first_step = y_t;
    x_first_step = x_t;
    while(y!=-1 && x!=-1){
        int y_next = map[y][x].y_v;
        int x_next = map[y][x].x_v;
        map[y][x].path=1;
        if(y_next!=-1 && x_next!=-1){
            y_third_step = y_second_step;
            x_third_step = x_second_step;
            y_second_step = y_first_step;
            x_second_step = x_first_step;
            y_first_step = y;
            x_first_step = x;
        } 
        y=y_next;
        x=x_next;
    }
}


void NavMap::purge(){
    avail.clear();
    for(int j=0; j<size_y; j++){
        for(int i=0; i<size_x; i++){
            map[j][i].toward=numeric_limits<uint16_t>::max();   //unne?
            map[j][i].heur=numeric_limits<uint16_t>::max();     //unne?
            map[j][i].y_v=-1;
            map[j][i].x_v=-1;
            map[j][i].heading_v=-1;
            map[j][i].visited=0;
            map[j][i].path=0;
        }
    }
}



class AStarPubSub
{
    //Class that reacts to receiving new rover coordinates and recomputes astar path when needed
    //Publishes the path as a sequence of points to pass through

    public:
        
        const vector<int> x_target_list = {50, 50, 45, 4};
        const vector<int> y_target_list = {10, 20, 30, 59};
        
        AStarPubSub(NavMap* x_map){
            pub = n.advertise<geometry_msgs::Quaternion>("/rover_next_pose", 1);
            sub = n.subscribe("/rover_coordinates_pose", 1, &AStarPubSub::call_a_star, this);
            sub2 = n.subscribe("/hal_next_target", 1, &AStarPubSub::change_target, this);
            xx_map=x_map;
            target_iterator = 0;
        }

        void call_a_star(const geometry_msgs::Pose2D::ConstPtr& msg)
        {
            //add coordinate filtration


            //translation
            float x_0 = msg->x;
            float y_0 = msg->y;

            float x_s_temp = -2*y_0+2;
            float y_s_temp = xx_map->size_y-2*x_0-3;   
            int x_s = round(x_s_temp);
            int y_s = round(y_s_temp);

            //safety
            if (x_s < 0) x_s=0;  
            if (y_s < 0) y_s=0;  
            if (x_s >= xx_map->size_x) x_s=xx_map->size_x-1;  
            if (y_s >= xx_map->size_y) y_s=xx_map->size_y-1;  

            bool to_recompute = (abs(y_s-xx_map->y_last_start) + abs(x_s-xx_map->x_last_start) >=2);
            //bool to_recompute=1;
            if(to_recompute){
                cout<<"Astar coord: y="<<int(y_s)<<", x="<<int(x_s)<<endl;
                float theta_0 = msg->theta;
                int head_s=0;
                if(-M_PI/8<=theta_0 && theta_0<=M_PI/8) head_s = 0;
                else if(-3*M_PI/8<=theta_0 && theta_0<=-M_PI/8) head_s = 1;
                else if(-5*M_PI/8<=theta_0 && theta_0<=-3*M_PI/8) head_s = 2;
                else if(-7*M_PI/8<=theta_0 && theta_0<=-5*M_PI/8) head_s = 3;
                else if(7*M_PI/8<=theta_0 || theta_0<=-7*M_PI/8) head_s = 4;
                else if(5*M_PI/8<=theta_0 && theta_0<=7*M_PI/8) head_s = 5;
                else if(3*M_PI/8<=theta_0 && theta_0<=5*M_PI/8) head_s = 6;
                else if(M_PI/8<=theta_0 && theta_0<=3*M_PI/8) head_s = 7;

                //separate switch for a star? so it does not change paths too rapidly
                xx_map->a_star(y_s, x_s, head_s);

                cout<<*xx_map<<endl;

                //Backwards translation
                float x_1 = (xx_map->size_y - 3 - xx_map->y_first_step) / 2;
                float y_1=(2 - xx_map->x_first_step)/2;
                float x_2=(xx_map->size_y - 3 - xx_map->y_second_step) / 2;  
                float y_2=(2 - xx_map->x_second_step)/2;
                float x_3=(xx_map->size_y - 3 - xx_map->y_third_step) / 2;  
                float y_3=(2 - xx_map->x_third_step)/2;
                //also using theta_0, x_0, y_0

                geometry_msgs::Quaternion next_pose_msg;
                next_pose_msg.x=x_2;
                next_pose_msg.y=y_2;
                next_pose_msg.z=x_3;
                next_pose_msg.w=y_3;

                pub.publish(next_pose_msg);
            }
        }

        void change_target(const std_msgs::String::ConstPtr& msg)
        {
            // xx_map->set_target(x_target_list[target_iterator], y_target_list[target_iterator]);
            // target_iterator++;
            // xx_map->compute_dist();  //To be restored
        }

    private:
        ros::NodeHandle n;
        ros::Publisher pub;
        ros::Subscriber sub;
        ros::Subscriber sub2;
        NavMap* xx_map;
        int target_iterator;

};



int main(int argc, char **argv){
    NavMap map;
    
    map.set_target(4, 59);      //temp
    map.compute_dist();         //temp
    
    ros::init(argc, argv, "a_star");
    AStarPubSub ps_obj(&map);
    
    
    ros::spin();
    
    return 0;
}