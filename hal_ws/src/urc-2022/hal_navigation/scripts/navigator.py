#!/usr/bin/env python3
import rospy
import os
import sys
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose2D, Quaternion
from std_msgs.msg import String


PID_FREQ = 100.0

# VERY important line, without it math_stuff import will be problematic
# because of ROS running script specifics
sys.path.append(os.path.dirname(__file__))

from commands import Commands

navi = Commands()

# rover current coordinates and next goals
x0 = None
y0 = None
xt1 = None
yt1 = None
xt2 = None
yt2 = None
theta = None

auto_mode = "IDLE"
wait_manual = 0

def pose_callback(msg):
    global x0, y0, theta
    x0 = msg.x
    y0 = msg.y
    theta = msg.theta

def target_callback(msg):
    global xt1, yt1, xt2, yt2
    xt1 = msg.x
    yt1 = msg.y
    xt2 = msg.z
    yt2 = msg.w

class COMMS:
    def __init__(self):
        self.pub = rospy.Publisher('/hal_next_target', String, queue_size = 1)
        self.sub_com = rospy.Subscriber("/hal_commands", String, self.comm_callback)

    def comm_callback(self, msg):
        global auto_mode, wait_manual
        if(msg.data=="START"):
            string_msg_move = "aaa"
            self.pub.publish(string_msg_move)
            rospy.sleep(5.0)
            auto_mode = "WORKING"
        elif(msg.data=="WAIT"):
            wait_manual = 1
            auto_mode = "WAITING"
        elif(msg.data=="RESUME"):
            if(not wait_manual):
                string_msg_move = "aaa"
                self.pub.publish(string_msg_move)
            wait_manual = 0
            rospy.sleep(5.0)
            auto_mode = "WORKING"
        elif(msg.data=="ABORT"):
            auto_mode = "IDLE"

class PID:
    def __init__(self):
        self.pub = rospy.Publisher('cmd_vel', Twist, queue_size = 1)

    def PID_controller(self, event=None):
        global navi, x0, y0, theta, xt, yt, auto_mode
        if auto_mode=="WORKING":
            if not [i for i in (x0, y0, theta, xt1, yt1, xt2, yt2) if i is None]:
                lin_vel, ang_vel = navi.goto(theta, x0, y0, xt1, yt1, xt2, yt2) 
                print(lin_vel, ang_vel)
                # if lin_vel != 0:                                          #---\\
                twist = Twist()
                twist.linear.x = lin_vel
                twist.angular.z = ang_vel
                self.pub.publish(twist)
                # else:
                #     auto_mode = "WAITING"                                 #the purpose of this is to stop the rover but it bugs
        else:
            twist = Twist()
            twist.linear.x = 0
            twist.angular.z = 0
            self.pub.publish(twist)

def main():
    rospy.init_node('navigator', anonymous=True)
    sub = rospy.Subscriber("/rover_coordinates_pose", Pose2D, pose_callback)
    sub = rospy.Subscriber("/rover_next_pose", Quaternion, target_callback)
    pid = PID()
    comms = COMMS()

    rospy.Timer(rospy.Duration(1.0/PID_FREQ), pid.PID_controller)
    rospy.spin()


if __name__ == "__main__":
    main()