#!/usr/bin/env python3
from math import atan2, sqrt, pi, cos
import rospy
from std_msgs.msg import Float32

class Commands:
    def __init__(self, max_lin_speed=1.5, max_ang_speed=4):   #was 2, 3
        # proportional gain in PID
        self.kp_ang = 5                         #was =7
        self.ki_ang = 3                         #was =1
        self.kd_ang = 0.05                      #was =0.07
        self.kd_i_reloader = 0.1               #was =1.5
        self.kp_lin = 2                  
        self.max_lin_speed = max_lin_speed
        self.max_ang_speed = max_ang_speed
        self.margin_of_error = 0.5          #changed to work with a star but not true!!!
        self.error_angle = 0
        self.integral_ang_error=0
        self.max_integral_ang_error=1
        self.period = 0.01
        self.prev_desired = 0
        self.pub = rospy.Publisher('dbg_pid', Float32 , queue_size = 1)

    def goto(self, current_angle, x0, y0, x1, y1, x2, y2):
        angular_vel = self.control_signal_angular(x0, y0, x1, y1, x2, y2, current_angle)
        linear_vel = self.control_signal_linear(x0, y0, x1, y1, x2, y2)
        return linear_vel, angular_vel

    ####### Angular speed control #######
    def desired_direction(self, xa, ya, xb, yb):    #what if x1=x2, y1=y2?
        alpha_d = atan2((yb-ya),(xb-xa))
        return alpha_d
    
    def control_signal_angular(self, x0, y0, x1, y1, x2, y2, current_angle):
        desired_angle = self.desired_direction(x0, y0, x2, y2)
        desired_heading = self.desired_direction(x1, y1, x2, y2)
        error = desired_angle - float(current_angle)
        if error > pi:
            error = error - 2*pi
        if error < -pi:
            error = error + 2*pi
        
        error_diff = error - self.error_angle
        desire_diff = desired_heading - self.prev_desired
        self.error_angle = error
        self.prev_desired = desired_heading

        self.integral_ang_error += self.period*error
        if self.integral_ang_error>self.max_integral_ang_error:
            self.integral_ang_error = self.max_integral_ang_error
        if self.integral_ang_error<-self.max_integral_ang_error:
            self.integral_ang_error = -self.max_integral_ang_error
        # d_in_pid = self.kd_ang * error_diff / self.period + self.kd_i_reloader * desire_diff / self.period
        # if d_in_pid != 0.0:
        #     self.pub.publish(d_in_pid)
        self.pub.publish(self.ki_ang * self.integral_ang_error)


        angular_speed = self.kp_ang * error + self.ki_ang * self.integral_ang_error + self.kd_ang * error_diff / self.period + self.kd_i_reloader * desire_diff / self.period

        if angular_speed > self.max_ang_speed:
            angular_speed = self.max_ang_speed
        if angular_speed < -self.max_ang_speed:
            angular_speed = -self.max_ang_speed
        
        if self.displacemant_error(x0, y0, x2, y2) == -1:
            angular_speed = 0

        return angular_speed
    ######################################

    ######## Linear speed control ########
    def displacemant_error(self, x0, y0, x2, y2):
        d = sqrt((x2-x0)**2+(y2-y0)**2)
        if d < self.margin_of_error:
            d = -1
        return d

    def control_signal_linear(self, x0, y0, x1, y1, x2, y2):
        error = self.displacemant_error(x0, y0, x2, y2)
        if error != -1:
            error = error * cos(self.error_angle)       #BE WARNED, MAY HIT STUFF BEHIND
            linear_speed = self.kp_lin * error
            
            if linear_speed > self.max_lin_speed:
                linear_speed = self.max_lin_speed
        else:
            linear_speed = 0
        return linear_speed
    ######################################