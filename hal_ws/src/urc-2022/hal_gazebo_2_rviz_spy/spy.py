#!/usr/bin/env python3
from re import X
import rospy
import os
import sys
from std_msgs.msg import Header
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, PoseStamped, Pose
from gazebo_msgs.msg import ModelStates
import numpy as np
import math

# VERY important line, without it math_stuff import will be problematic
# because of ROS running script specifics
sys.path.append(os.path.dirname(__file__))

pos = np.array([0.0, 0.0, 0.0])
orient = np.array([0.0,0.0,0.0,0.0]) #w x y z

def gazebo_callback(msg):
    global pos, orient
    i = 0
    for name in msg.name:
        if name == "hal":
            break
        i+=1
    if i != len(name):
        pos[0] = msg.pose[i].position.x
        pos[1] = msg.pose[i].position.y
        pos[2] = 0
        orient[0] = msg.pose[i].orientation.w
        orient[1] = msg.pose[i].orientation.x
        orient[2] = msg.pose[i].orientation.y
        orient[3] = msg.pose[i].orientation.z
    else:
        rospy.logwarn("No model named \"hal\" in simulation")


def main():
    rospy.init_node('spy', anonymous=True)
    global now
    now = rospy.get_rostime()
    rate = rospy.Rate(10)
    sub = rospy.Subscriber("/gazebo/model_states", ModelStates, gazebo_callback)
    pub = rospy.Publisher("/real_pose", PoseStamped, queue_size=10)

    # rate.sleep()
    # rospy.spin()
    global pos, orient
    while not rospy.is_shutdown():
        out_msg = PoseStamped()
        out_msg.header.frame_id = "world"
        out_msg.header.stamp = rospy.Time.now()
        out_msg.pose.position.x = pos[0]
        out_msg.pose.position.y = pos[1]
        out_msg.pose.position.z = pos[2]
        out_msg.pose.orientation.w = orient[0]
        out_msg.pose.orientation.x = orient[1]
        out_msg.pose.orientation.y = orient[2]
        out_msg.pose.orientation.z = orient[3]
        pub.publish(out_msg)
        rate.sleep()


if __name__ == "__main__":
    main()