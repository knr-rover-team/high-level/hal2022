#!/usr/bin/env python3
from math import pi, radians, sqrt, sin, cos, atan2, acos, radians, degrees
from tf.transformations import euler_from_quaternion
import numpy as np


class NavigationMath:
    """
    Using only SI units without prefixes!!! Not 'km', but 'm' instead
    """

    def __init__(self):
        self.local_radius = 6371000
        self.Rx = lambda q: np.matrix([[1, 0, 0], 
                                    [0, cos(q), -sin(q)],
                                    [0, sin(q), cos(q)]])
        self.Ry = lambda q: np.matrix([[cos(q), 0, -sin(q)], 
                                    [0, 1, 0], 
                                    [sin(q), 0, cos(q)]])
        self.Rz = lambda q: np.matrix([[cos(q), -sin(q), 0], 
                                    [sin(q), cos(q), 0], 
                                    [0, 0, 1]])
        self.lat0 = None
        self.lon0 = None

    def geo_to_cartesian(self, origin_point, points):
        cartesian_points = []
        for point in points:
            x = (point[0]-origin_point[0]) * \
                (self.kielcensis_radius*2*pi/360)
            y = (point[1]-origin_point[1])*(self.earth_radius*2*pi/360)
            cartesian_points.append([x, y])
        return cartesian_points

    def magnetic_direction(self, w_param):
        magnetic_angle = 2*acos(w_param)
        return magnetic_angle

    def global_ar_position(self, yaw, ar_cam_vector, ar_id, ar_vecs_glob):
        """
        Returns position of ar code in camera in global marsyard reference frame
        """
        # zeroing y component because it's irrelevant
        ar_cam_vector[1] = 0
        ar_cam_vector = np.array(ar_cam_vector).reshape(3,1)
        cam_vec_to_rover = self.Rz(-radians(90))*self.Rx(-radians(90))*ar_cam_vector
        cam_vec_to_glob = self.Rz(yaw)*cam_vec_to_rover
        for i in range(len(ar_vecs_glob)):
            if ar_id==ar_vecs_glob[i][0]:
                ar_vec_glob = np.matrix([[ar_vecs_glob[i][1]],[ar_vecs_glob[i][2]],[0]])
                rover_vec_glob = ar_vec_glob - cam_vec_to_glob
                x_glob = rover_vec_glob[0]
                y_glob = rover_vec_glob[1]
                return [x_glob, y_glob, 0]
        return None

    def euler_angles(self, x, y, z, w):
        """
        Returns list of euler angles (roll, pitch, yaw)
        """
        angles = euler_from_quaternion([x, y, z, w])
        return angles
    
    # GPS methods for Gazebo simulator
    def set_gps_origin(self, lat0, lon0):
        self.lat0 = radians(lat0)
        self.lon0 = radians(lon0)

    def haversine_dist(self, lat1, lon1):
        """
        http://www.movable-type.co.uk/scripts/latlong.html
        Calculation of distance based on haversine formula (assumption
        of spherical Earth - error should be on average under 0.5%). 
        Conversion to local 2D cartesian coordinate system. 
        """
        lat1 = radians(lat1)
        lon1 = radians(lon1)
        delta_fi = lat1 - self.lat0
        delta_lambda = lon1 - self.lon0
        a = sin(delta_fi/2.)**2 + cos(self.lat0) * cos(lat1) * sin(delta_lambda/2.)**2
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        d = self.local_radius * c
        return d
     
    def bearing(self, lat1, lon1):
        """
        Calculate direction from starting position.
        """
        lat1 = radians(lat1)
        lon1 = radians(lon1)
        delta_lambda = lon1 - self.lon0
        y = sin(delta_lambda * cos(lat1))
        x = cos(self.lat0) * sin(lat1) - sin(self.lat0) * cos(lat1) * cos(delta_lambda)
        fi = degrees(atan2(y, x))
        return fi

    def get_xy(self, lat1, lon1):
        d = self.haversine_dist(lat1, lon1)
        alpha = self.bearing(lat1, lon1)
        x = d * cos(radians(alpha))
        y = - d * sin(radians(alpha))
        return [x, y]
