#!/usr/bin/env python3

# origin of global cartesian coordinate, should be specified in global [North, East]
# with frictional part in decimal system
origin_geo = [50.175863, 19.742935]

rover_pos = [50.878248, 20.642365]

# ar_codes_geo should be specified in global [North, East]
# with frictional part in decimal system
ar_codes_geo = [[50.878254, 20.642255],
                [50.878237, 20.642416]]

######################## GAZEBO COORDINATES ########################
origin_gaz = [0, 0]
rover_pos_gaz = [0, 0]
# ar_codes_gaz = [[125, 1, 0],
#                 [13, 1, -1],
#                 [33, 3, -3]]
# ar_codes_gaz = [[125, 2, 0],
#                 [13, 4, -3],
#                 [33, 7, -7]]
ar_codes_gaz = [[125, 2, 0],
                [13, 7, -10],
                [33, 12, -18]]  #12 -16

set_starting_position = 1
