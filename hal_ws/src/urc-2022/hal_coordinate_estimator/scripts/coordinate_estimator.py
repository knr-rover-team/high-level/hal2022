#!/usr/bin/env python3
import rospy
import os
import sys
from hal_ar_codes_recognition.msg import ArPosition
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Pose2D
from sensor_msgs.msg import Imu
from sensor_msgs.msg import NavSatFix

# VERY important line, without it math_stuff import will be problematic
# because of ROS running script specifics
sys.path.append(os.path.dirname(__file__))

import globals as gl
from math_stuff import NavigationMath

nm = None
angles = None
pub_ar = None
pub_gps = None

def ar_callback(msg):
    global angles, nm, pub_ar
    if angles is not None:
        rover_global = nm.global_ar_position(angles[2],[msg.x, msg.y, msg.z], msg.id, gl.ar_codes_gaz)
        # rospy.loginfo(rover_global)
        if rover_global is not None:
            coordinates = Vector3()
            coordinates.x = rover_global[0]
            coordinates.y = rover_global[1]
            coordinates.z = rover_global[2]
            pub_ar.publish(coordinates)


def imu_callback(msg):
    global nm, angles
    angles = nm.euler_angles(msg.orientation.x, msg.orientation.y,
                             msg.orientation.z, msg.orientation.w)

def gps_callback(msg):
    global nm, pub_gps
    if gl.set_starting_position:
        nm.set_gps_origin(msg.latitude, msg.longitude)
        gl.set_starting_position = 0
    else:
        coordinates = nm.get_xy(msg.latitude, msg.longitude)
        print("x:{0:.2f}  y:{1:.2f}".format(coordinates[0], coordinates[1]))
        if angles is not None and coordinates is not None:
            pose2d = Pose2D()
            pose2d.x = coordinates[0]
            pose2d.y = coordinates[1]
            pose2d.theta = angles[2]
            pub_gps.publish(pose2d)


def main():
    global nm, pub_ar, pub_gps
    nm = NavigationMath()

    pub_ar = rospy.Publisher('rover_coordinates_ar', Vector3, queue_size=10)
    pub_gps = rospy.Publisher('rover_coordinates_pose', Pose2D, queue_size=10)
    rospy.init_node('position_estimation', anonymous=True)
    sub_ar = rospy.Subscriber(
        "/ar_position", ArPosition, ar_callback)
    sub_imu = rospy.Subscriber("/imu", Imu, imu_callback)
    sub_gps = rospy.Subscriber("/gps", NavSatFix, gps_callback)

    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        rate.sleep()


if __name__ == "__main__":
    main()
