Launch: "roslaunch hal_manipulator hal_manipulator.launch"

Move joints: use command in "command_move_joint.txt"

REQUIRED: update "meshes_gazebo" folder from GDrive:
https://drive.google.com/drive/folders/13qJMbTBdVsYpLUEaTaGURbn61u7cH90l?usp=sharing
