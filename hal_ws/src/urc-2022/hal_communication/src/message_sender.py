#!/usr/bin/env python3
import rospy
import math
from geometry_msgs.msg import Twist
from hal_msgs.srv import communication, communicationRequest, communicationResponse


# TODO
WHEEL_RAD = 0.1575
ROVER_WIDTH = 0.91


def spd_callback(msg):
    spd_lin = msg.linear.x  #m/s
    spd_ang = msg.angular.z #rad/s
    spd_l = ( spd_lin - spd_ang * ROVER_WIDTH/2 ) / WHEEL_RAD   #rad/s
    spd_r = ( spd_lin + spd_ang * ROVER_WIDTH/2 ) / WHEEL_RAD   #rad/s
    # maiboard unit
    spd_mb_unit_l = round( spd_l / 2 / math.pi * 100 ) # % of rotation/s
    spd_mb_unit_r = round( spd_r / 2 / math.pi * 100 ) # % of rotation/s
    rospy.loginfo("Sending speed L: %d R: %d", spd_mb_unit_l, spd_mb_unit_r)
    
    #encode and send message
    rospy.wait_for_service('/comm_server')
    try:
        service_proxy = rospy.ServiceProxy('/comm_server', communication)
        service_request = communicationRequest()
        service_request.comm_mode = 0
        service_request.id = 20
        service_request.data = [spd_mb_unit_l & 0xff, spd_mb_unit_r & 0xff]
        service_response = service_proxy(service_request)
        service_proxy.close()
        rospy.loginfo("Service response: {}".format(service_response))
    except rospy.ServiceException as e:
        rospy.logerr("Service call failed: {}".format(e))


def message_sender():
    rospy.init_node('message_sender', anonymous=True)
    sub = rospy.Subscriber("/cmd_vel", Twist, spd_callback)
    rospy.spin()

if __name__ == '__main__':
    message_sender()