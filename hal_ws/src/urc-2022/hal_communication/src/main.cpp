#include <exception>
#include <iostream>

#include "comm_server.h"

/**
 * @brief Function prints red fromatted ROS-style error mesage
 * @param ch error message
 */
void printError(const char *ch) {
  std::cerr << "\033[0;31m";
  std::cerr << "[ ERROR] [" << ros::Time::now()
            << "] Communication node failed with message: " << ch;
  std::cerr << "\033[0m\n";
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "hal_communication");
  try {
    auto cs = HAL::CommunicationServer();
    ros::spin();
  } catch (const std::runtime_error &ex) {
    // ROS_ERROR doesn't work here because it's after ros::shutdown()
    printError(ex.what());
    return 1;
  }
  return 0;
}