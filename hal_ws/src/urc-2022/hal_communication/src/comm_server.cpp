#include "comm_server.h"

#include <sys/socket.h>

#include <iomanip>
#include <sstream>
#include <string>

namespace HAL {

TCPClient::TCPClient(const TCPParams &serverParams)
    : serverParams_(serverParams) {}

TCPClient::~TCPClient() { close(sockFd_); }

auto TCPClient::initConnection_() -> std::optional<std::string> {
  // initialize server address
  servAddr_.sin_family = AF_INET;
  servAddr_.sin_port =
      htons(serverParams_.port);  // htons sets port in network byte order
  if (inet_pton(AF_INET, serverParams_.IP.c_str(), &servAddr_.sin_addr) <= 0) {
    return "Invalid IP address";
  }

  // get socket file descriptor
  sockFd_ = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (sockFd_ < 0) {
    return "Cannot create the socket";
  }

  int attempt_count = 0;
  int status = -1;
  while (status != 0 && attempt_count < comm::CONST ::MAX_ATTEMPT_COUNT) {
    status = connect(sockFd_, (struct sockaddr *)&servAddr_, sizeof(servAddr_));
    attempt_count++;
  }
  if (status < 0) {
    return std::string("Cannot connect to the rover after ") +
           std::to_string(attempt_count) + std::string(" attempts");
  }
  return {};
}

auto TCPClient::restartConnection() -> std::optional<std::string> {
  close(sockFd_);
  auto res = initConnection_();
  if (res.has_value()) {
    return res.value();
  }
  return {};
}

auto TCPClient::sendMessage(const std::string &data)
    -> std::optional<std::string> {
  if (sockFd_ == 0) {
    // socked was not yet initialized
    auto res = initConnection_();
    if (res.has_value()) {
      return res.value();
    }
  }
  auto res = send(sockFd_, data.c_str(), strlen(data.c_str()), 0);
  if (res < 0) {
    return "Cannot send data to the rover";
  }

  return {};
}

CommunicationServer::CommunicationServer()
    : nh_(ros::NodeHandle()),
      server_(nh_.advertiseService(comm::CONST ::SRV_NAME,
                                   &CommunicationServer::sendData, this)) {
  TCPParams params;
  auto result = readConnectionParams_(params);
  if (result.has_value()) {
    throw std::runtime_error((*result).c_str());
  }
  tcpHandler_ = TCPClient(params);
}

auto CommunicationServer::convert2Hex_(int dec_num) -> std::string {
  std::stringstream stream;
  stream << std::uppercase << std::hex << dec_num;
  std::string result(stream.str());
  return result;
}

auto CommunicationServer::convert2Standard_(uint8_t id,
                                            std::vector<uint8_t> &data)
    -> std::string {
  std::string result;
  result = "#";  // # starts the frame
  data.insert(data.begin(), id);
  for (auto element : data) {
    auto hex_str = convert2Hex_(element);
    if (hex_str.size() == 1) {
      // for one-positional hex numbers
      hex_str.insert(0, 1, '0');
    }
    if (hex_str.size() != 2) {
      // when single hex is longer/shorter than 2 signs
      throw std::runtime_error("Message conversion error");
    }
    result.append(hex_str);
  }
  // for fills remaining fields with 'x' according to standard
  for (int i = result.size(); i < comm::CONST ::COMMUNICATION_FRAME_SIZE; i++) {
    result.append("x");
  }
  return result;
}

auto CommunicationServer::readConnectionParams_(TCPParams &params)
    -> std::optional<std::string> {
  if (!nh_.getParam(comm::CONST ::PARAM_PREFIX + std::string("/") +
                        comm::CONST ::PARAM_IP,
                    params.IP)) {
    return "No rover's IP on parameter server";
  }
  if (!nh_.getParam(comm::CONST ::PARAM_PREFIX + std::string("/") +
                        comm::CONST ::PARAM_PORT,
                    params.port)) {
    return "No rover's port on parameter server";
  }
  return {};
}

bool CommunicationServer::sendData(hal_msgs::communication::Request &req,
                                   hal_msgs::communication::Response &resp) {
  if (req.comm_mode == req.TCPIP) {
    auto res = tcpHandler_.sendMessage(convert2Standard_(req.id, req.data));
    if (res.has_value()) {
      resp.success = false;
      resp.message = res.value();
      return true;
    }
  } else if (req.comm_mode == req.BLUETOOTH) {
  } else {
    resp.success = false;
    resp.message = "No appropriate 'comm_mode' field in request";
    return false;
  }
  resp.success = true;
  return true;
}

}  // namespace HAL
