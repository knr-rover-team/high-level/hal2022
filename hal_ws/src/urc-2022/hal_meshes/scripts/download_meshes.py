#!venv/bin/python
import gdown
import rospkg
import os

MESH_PACKAGE = 'hal_meshes'
url = 'https://drive.google.com/drive/folders/1hhFKyGp6KSxXGdzLcGORk2UVR6Hrezgg'
FOLDER_NAME = 'HAL-062\ MODEL'
SUBFOLDERS = ['meshes', 'meshes_manipulator']
MESHES_MAP_SUBFOLDER = 'meshes_map'

rospack = rospkg.RosPack()

gdown.download_folder(url, quiet=True, use_cookies=False)

for i in range(len(SUBFOLDERS)):
	cmd = 'cp -r ' + os.path.join(FOLDER_NAME, SUBFOLDERS[i]) + ' ' + os.path.join(rospack.get_path(MESH_PACKAGE))
	os.popen(cmd)

cmd = 'cp -r ' + os.path.join(FOLDER_NAME, MESHES_MAP_SUBFOLDER) + ' ' + os.path.join(
    rospack.get_path(MESH_PACKAGE), 'models')
os.popen(cmd)

cmd = 'rm -r ' + FOLDER_NAME
os.popen(cmd)
