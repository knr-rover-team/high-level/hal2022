#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from hal_ar_codes_recognition.msg import ArPosition
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import os

# VERY important line, without it aruco import will be problematic 
# because of ROS running script specifics
sys.path.append(os.path.dirname(__file__))

from aruco import Ar


#aruco class
ar_finder = None
#cv bridge
bridge = None
#camera data
screen_g=None
camera_activated=None

pub = None

def image_callback(img_msg):
    # rospy.loginfo(img_msg.header)
    global bridge
    try:
        # convert imgmsg to opencv format, encoding="passthrough" -
        # - image not changed
        cv_image = bridge.imgmsg_to_cv2(img_msg, "passthrough")
    except CvBridgeError as e:
        rospy.logerr("CvBridge Error: {0}".format(e))
    #returning AR tag postion
    global ar_finder
    poses, ids = ar_finder.get_ar_pose(cv_image)
    global screen_g
    screen_g=cv_image
    global camera_activated
    camera_activated=1
    #printing AR tag postion in the console
    if poses is not None:
        for i in range(len(poses)):
            ar_message = ArPosition()
            ar_message.x = poses[i][0]
            ar_message.y = poses[i][1]
            ar_message.z = poses[i][2]
            ar_message.id = ids[i]
            global pub
            pub.publish(ar_message)
    # rospy.loginfo(poses)

def main():
    global pub
    pub = rospy.Publisher('ar_position', ArPosition, queue_size=10)
    # starting ROS node
    rospy.init_node('opencv_node', anonymous=True)
    rospy.loginfo("Hello ROS!")
    # declaring usage of CvBridge in order to use openCv in ROS
    global bridge
    bridge = CvBridge()
    # initialization of AR codes finder
    global ar_finder
    #aruco class initialization with camera settings

    #MARKER SIZE IMPORTANT IS;
    #CAMERA CAlIBRATION REQUIRED

    ar_finder = Ar(marker_length=0.2, frame_width=1280, frame_height=720,
                   horizontal_fov=1.9, k1=-0.0435, k2=0.0119, k3=-0.0054, p1=-0.0008, p2=-0.0003)
    # initialization of subscriber
    sub_image = rospy.Subscriber(
        "/camera/image_raw", Image, image_callback)
    # creation of window to display frames from camera plugin from gazebo
    cv2.namedWindow("Simulated camera")
    rate=rospy.Rate(50)
    global screen_g
    global camera_activated
    while not rospy.is_shutdown():
        #imshow function CAN'T be inside of the subscriber function
        #due to that fact rospy.Rate and rate.sleep was needed to activ while loop
        if camera_activated:
            cv2.imshow("Simulated camera", ar_finder.set_mark_ar_tag(screen_g))
            cv2.waitKey(1)
        rate.sleep()


if __name__ == "__main__":
    main()
