#!/usr/bin/env python3
import cv2
import numpy as np
import math


class Ar:
    def __init__(self, marker_length, frame_width, frame_height, horizontal_fov, k1, k2, k3, p1, p2):
        # horizontal_fov is horizontal field of view, is defined as parameter of
        # gazebo plugin
        self.ar_dict = cv2.aruco.getPredefinedDictionary(
            cv2.aruco.DICT_5X5_1000)
        self.parameters = cv2.aruco.DetectorParameters_create()
        self.marker_length = marker_length
        center_x = frame_width/2
        center_y = frame_height/2
        # focal_length calculated from pinhole camera model
        focal_length = 0.5 * frame_width / math.tan(0.5 * horizontal_fov)
        self.camera_matrix = np.array([[focal_length, 0, center_x],
                                       [0, focal_length, center_y],
                                       [0, 0, 1]], np.float)
        self.dist_coeffs = np.array([k1, k2, p1, p2, k3], np.float)

    def get_ar_pose(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(
            gray, self.ar_dict, parameters=self.parameters)
        translations = []
        ids_ret = []
        if ids is not None:
            for i in range(len(ids)):
                _, translation, _ = cv2.aruco.estimatePoseSingleMarkers(
                    corners, self.marker_length, self.camera_matrix, self.dist_coeffs)
                translations.append(translation[i][0])
            for i in range(len(ids)):
                ids_ret.append(ids[i][0])
        return translations, ids_ret

    def set_mark_ar_tag(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(
            gray, self.ar_dict, parameters=self.parameters)
        if corners is not None and ids is not None:
            # Extract cornes of AR tag to seperate variables
            for (markerCorners, markerIds) in zip(corners, ids):
                corners = markerCorners.reshape((4, 2))
                (topLeft, topRight, bottomRight, bottomLeft) = corners
                # Convert data to int
                topLeft = (int(topLeft[0]), int(topLeft[1]))
                topRight = (int(topRight[0]), int(topRight[1]))
                bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
                bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
            # Draw the bounding box of AR tag
            cv2.line(frame, topLeft, topRight, (0, 255, 0), 2)
            cv2.line(frame, topRight, bottomRight, (0, 255, 0), 2)
            cv2.line(frame, bottomRight, bottomLeft, (0, 255, 0), 2)
            cv2.line(frame, bottomLeft, topLeft, (0, 255, 0), 2)
            # Draw center of AR tag
            cX = int((topLeft[0] + bottomRight[0])/ 2.0)
            cY = int((topLeft[1] + bottomRight[1])/ 2.0)
            cv2.circle(frame, (cX, cY), 4, (0, 0, 255), -1)

            # draw ArUco marker ID
            cv2.putText(frame, str(markerIds), (int(topLeft[0]), int(topLeft[1]) - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        return frame
