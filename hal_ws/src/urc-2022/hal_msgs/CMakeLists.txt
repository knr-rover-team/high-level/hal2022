cmake_minimum_required(VERSION 3.0.2)
project(hal_msgs)

find_package(catkin REQUIRED
  message_generation
  genmsg
  actionlib_msgs
  std_msgs
)

# Generate messages in the 'msg' folder
add_message_files(
  DIRECTORY msgs
  FILES
)

# Generate services in the 'services' folder
add_service_files(
  DIRECTORY services
  FILES
  communication.srv
)

# Generate actions in the 'action' folder
add_action_files(
  DIRECTORY actions
  FILES
)

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
  actionlib_msgs
)

catkin_package(
  CATKIN_DEPENDS message_runtime std_msgs actionlib_msgs
)

