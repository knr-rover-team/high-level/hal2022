#!/bin/bash

if  ! test -f /etc/udev/rules.d/49-gps-rules.rules
then
    sudo touch /etc/udev/rules.d/49-gps-rules.rules
    chmod 777 /etc/udev/rules.d/49-gps-rules.rules
    # idVendor and idProduct must match with the UART-USB converter info
    echo 'KERNEL=="ttyACM[0-9]*", SUBSYSTEM=="tty", ATTRS{idVendor}=="####", ATTRS{idProduct}=="####", SYMLINK="ttyGPS"' > /etc/udev/rules.d/49-gps-rules.rules 
fi

sudo chmod 777 /dev/ttyGPS