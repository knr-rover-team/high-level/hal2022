#!/usr/bin/env python3
import rospy
import serial
from sensor_msgs.msg import NavSatFix, NavSatStatus
from std_msgs.msg import Header

class GpsNode:
    def __init__(self):
        self.port = "/dev/ttyGPS"   # Plug GPS and run set_tty.sh
        self.publisher = rospy.Publisher('/gps', NavSatFix, queue_size=10)
        rospy.Timer(rospy.Duration(nsecs=1), self.gps_publish)
        self.lon = 0.0
        self.lat = 0.0
        self.alt = 0.0
        self.hdop = 0.0
        self.vdop = 0.0

    def gps_publish(self, any):
        msg = NavSatFix()
        gps_params = self.gps_grabber()
        msg.header = gps_params.get('head')
        msg.status = gps_params.get('stat')
        msg.latitude = self.lat
        msg.longitude = self.lon
        msg.altitude = self.alt
        
        msg.position_covariance[0] = round(pow(self.hdop,2),4)
        msg.position_covariance[4] = round(pow(self.hdop,2),4)
        msg.position_covariance[8] = round(pow(self.vdop,2),4)
        msg.position_covariance_type = NavSatFix.COVARIANCE_TYPE_APPROXIMATED
        self.publisher.publish(msg)
        return any
    
    def gsa(self, data: list):
        self.hdop = float(data[16])
        self.vdop = float(data[17][0:3])

    def gga(self, data: list):
        lat_list: list = list(data[2])
        lon_list: list = list(data[4])
        lat_list.remove(".")
        lat_list.insert(2,".")
        lat_str = "".join(lat_list)
        lon_list.remove(".")
        lon_list.insert(3,".") 
        lon_str = "".join(lon_list)
        
        self.lat = round(float(lat_str),4)
        self.lon = round(float(lon_str),4)
        self.alt = float(data[9])



    def gps_grabber(self):
        self.got_info = False
        got_gsa = False
        got_gga = False
        head = Header()
        status = NavSatStatus()
        head.frame_id = "GPGGA/GPGSA"
        status.service = 1
        device = serial.Serial(self.port, baudrate=57600)
        try:
            while not got_gsa:
                line = str(device.readline())
                if line[2:8] == "$GPGSA":
                    data = line.split(',')
                    self.gsa(data)
                    got_gsa = True
            while not got_gga:
                line = str(device.readline())
                if line[2:8] == "$GPGGA":
                    data = line.split(',')
                    self.gga(data)
                    got_gga = True

            status.status = 0
            self.time = 0

            del device

        except:
            rospy.logwarn("GPS Connection Lost")
            status.status = -1
            del device
            rospy.sleep(duration=0.5)
        return {'head':head, 'stat':status}
    

if __name__ == '__main__':
    rospy.init_node('gps_node')
    GpsNode()
    rospy.spin()
    