#!/usr/bin/env python3
from re import X
import rospy
import os
import sys
from std_msgs.msg import Header
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, PoseStamped, Pose, Twist
import numpy as np
import math

# VERY important line, without it math_stuff import will be problematic
# because of ROS running script specifics
sys.path.append(os.path.dirname(__file__))

x_n = np.array([0, 0, 0, 0, 0, 0])  #estimate, initial
P_n = np.array([[1, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0],
                [0, 0, 0, 1, 0, 0],
                [0, 0, 0, 0, 1, 0],
                [0, 0, 0, 0, 0, 1]])*100    #estimate, initial

H = np.array([[1, 0, 0, 0, 0, 0],
              [0, 1, 0, 0, 0, 0]])  #when we measure AR position
H2 = np.array([[0, 0, 0, 1, 0, 0],
               [0, 0, 0, 0, 1, 0]]) #using speed orders
acc_mod = [0,0,0]
acc = [0,0,0]

pos = np.array([0,0])
spd = np.array([0,0,0])
new_measure = False

R = [[0.0001, 0],       #for AR position
     [0, 0.0001]]        #1 cm for now

#R2 could be dynamic
R2 = [[0.0004, 0],      #for driving speed
      [0, 0.0004]]       #0.2m/s for now. thats a low precision


quat = np.array([0, 0, 0, 0])

def quaternion_rotation_matrix(Q):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = Q[0]
    q1 = Q[1]
    q2 = Q[2]
    q3 = Q[3]
     
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
     
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
     
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
     
    # 3x3 rotation matrix
    rot_matrix = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
                            
    return rot_matrix


def imu_callback(msg):
    global x_n, P_n, H, acc, acc_mod, R, pos, new_measure, quat, R2, H2
    # convert acc vector to earth coordinate system

    acc_prev = acc
    # s_acc_x=(msg.linear_acceleration.x+0.025)*0.9982
    # s_acc_y=(msg.linear_acceleration.y-0.08)*0.9977
    # s_acc_z=(msg.linear_acceleration.z+0.235)*0.9792
    s_acc_x=msg.linear_acceleration.x
    s_acc_y=msg.linear_acceleration.y
    s_acc_z=msg.linear_acceleration.z
    s_acc=np.array([s_acc_x, s_acc_y, s_acc_z])
    # bc it is reported in a wrong order...
    # quat_x=-msg.orientation.y
    # quat_y=-msg.orientation.z
    # quat_z=msg.orientation.x
    # quat_w=msg.orientation.w
    #for sim
    quat_x=msg.orientation.x
    quat_y=msg.orientation.y
    quat_z=msg.orientation.z
    quat_w=msg.orientation.w
    quat=np.array([quat_w, quat_x, quat_y, quat_z])
    rot_matrix = quaternion_rotation_matrix(quat)
    # xtra_rot = np.array([[1.0000000,  0.0000000,  0.0000000],
    #                      [0.0000000,  0.9998875,  0.0149994],
    #                      [0.0000000, -0.0149994,  0.9998875]])
    # rot_matrix = np.matmul(rot_matrix, xtra_rot)                  
    acc=np.matmul(np.transpose(rot_matrix), s_acc)
    # acc[2]+=9.807
    acc[2]-=9.807

    # acc[0]-=0.01
    # acc[1]-=0.16
    # acc[2]+=0.39

    # dynamic calibration (drift eliminator)

    drift_multiplier = 2*10

    for i in range(3):
        if acc[i]+acc_mod[i]>0 and acc_prev[i]>0:
            acc_mod[i]-=(acc[i]+acc_mod[i]+acc_prev[i])/drift_multiplier
        elif acc[i]+acc_mod[i]<0 and acc_prev[i]<0:
            acc_mod[i]-=(acc[i]+acc_mod[i]+acc_prev[i])/drift_multiplier
        acc[i]+=acc_mod[i]

    # print("acc: ", '%.2f'%acc[0],'\t', '%.2f'%acc[1],'\t', '%.2f'%acc[2])
    # print('%.2f'%acc_mod[0],'\t', '%.2f'%acc_mod[1],'\t', '%.2f'%acc_mod[2])

    #time

    global now
    now_prev = now
    now = rospy.get_rostime()
    dt = now - now_prev
    dt_v = dt.to_sec()
    # print(dt.to_sec())

    #F matrix
    F = np.array([[1, 0, 0, dt_v, 0, 0],
                  [0, 1, 0, 0, dt_v, 0],
                  [0, 0, 1, 0, 0, dt_v],
                  [0, 0, 0, 1, 0, 0],
                  [0, 0, 0, 0, 1, 0],
                  [0, 0, 0, 0, 0, 1]])

    #G matrix
    dt2_2 = pow(dt_v, 2)/2
    G = np.array([[dt2_2, 0, 0],
                  [0, dt2_2, 0],
                  [0, 0, dt2_2],
                  [dt_v, 0, 0],
                  [0, dt_v, 0],
                  [0, 0, dt_v]])

    #predictor

    #control aka acceleration
    u_n = np.array(acc)

    x_n_1 = x_n
    P_n_1 = P_n

    #state extrapolation
    x_n = np.matmul(F, x_n_1) + np.matmul(G, u_n)

    #process noise
    Q = np.matmul(G, np.transpose(G))*0.01  #the precison of accelerometer. but there is convertion.
    #was: 0.001539

    #cov extrapolation
    P_n = np.matmul(np.matmul(F, P_n_1), np.transpose(F)) + Q


    if new_measure:     #using ar position
        #Kalman gain
        K_subcomp = np.matmul(np.matmul(H, P_n), np.transpose(H)) + R
        K_n = np.matmul(np.matmul(P_n, np.transpose(H)), np.linalg.inv(K_subcomp))
        new_measure=False

        #state update
        x_n = x_n + np.matmul(K_n, (pos - np.matmul(H, x_n)))


        #cov update
        sth = np.eye(6)-np.matmul(K_n, H)
        P_n = np.matmul(np.matmul(sth, P_n), np.transpose(sth)) + \
              np.matmul(np.matmul(K_n, R), np.transpose(K_n))
    else:               #using speed of rover
        # K_n=[[0, 0],
        #      [0, 0],
        #      [0, 0],
        #      [0, 0],
        #      [0, 0],
        #      [0, 0]]
        #Kalman gain
        K_subcomp = np.matmul(np.matmul(H2, P_n), np.transpose(H2)) + R2
        K_n = np.matmul(np.matmul(P_n, np.transpose(H2)), np.linalg.inv(K_subcomp))
        new_measure=False

        #state update
        x_n = x_n + np.matmul(K_n, ([spd[0], spd[1]] - np.matmul(H2, x_n)))


        #cov update
        sth = np.eye(6)-np.matmul(K_n, H2)
        P_n = np.matmul(np.matmul(sth, P_n), np.transpose(sth)) + \
              np.matmul(np.matmul(K_n, R2), np.transpose(K_n))



    
    
    print("pos: ", '%.2f'%x_n[0],'\t', '%.2f'%x_n[1],'\t', '%.2f'%x_n[2],'\n'
          "vel: ", '%.2f'%x_n[3],'\t', '%.2f'%x_n[4],'\t', '%.2f'%x_n[5],'\n'
          "K_n: ", K_n[0],'\n', K_n[1],'\n', K_n[2],'\n', K_n[3],'\n', K_n[4],'\n', K_n[5])



def ar_callback(msg):
    global pos, new_measure
    pos[0] = msg.x
    pos[1] = msg.y
    new_measure = True


def spd_callback(msg):
    global spd, quat
    fwd_spd = np.array([0.4*msg.linear.x,0,0])      #that 0.4 is from identification. It may be a problem.
                                                    #idk what it is. Also, prefferably, it should be the data from wheels
    rot_matrix = quaternion_rotation_matrix(quat)
    spd=np.matmul(rot_matrix, fwd_spd)
    # print("spd: ", '%.2f'%spd[0],'\t', '%.2f'%spd[1],'\t', '%.2f'%spd[2])


def main():
    rospy.init_node('kalman', anonymous=True)
    global now
    now = rospy.get_rostime()
    rate = rospy.Rate(200)
    # sub = rospy.Subscriber("/imu/data", Imu, imu_callback)    #real
    sub = rospy.Subscriber("/imu", Imu, imu_callback)           #gazebo
    sub2 = rospy.Subscriber("/rover_coordinates_ar", Vector3, ar_callback)
    sub3 = rospy.Subscriber("/cmd_vel", Twist, spd_callback)
    pub = rospy.Publisher("/kalman_pose", PoseStamped, queue_size=10)

    # rate.sleep()
    # rospy.spin()
    global x_n, quat
    while not rospy.is_shutdown():
        out_msg = PoseStamped()
        out_msg.header.frame_id = "world"
        out_msg.header.stamp = rospy.Time.now()
        out_msg.pose.position.x = x_n[0]
        out_msg.pose.position.y = x_n[1]
        out_msg.pose.position.z = 0
        out_msg.pose.orientation.w = quat[0]
        out_msg.pose.orientation.x = quat[1]
        out_msg.pose.orientation.y = quat[2]
        out_msg.pose.orientation.z = quat[3]
        pub.publish(out_msg)
        rate.sleep()


if __name__ == "__main__":
    main()